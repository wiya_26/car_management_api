const model = require('../models');

module.exports = {
  getAll: () => model.router.findAll(),
  findById: (id) => model.router.findOne({ where: { id: id } }),
  create: (data) => model.router.create(data),
  update: (data) =>
    model.router.update(
      {
        name: data.name,
        price: data.price,
        size: data.size,
        images: data.filename,
      },
      {
        where: {
          id: data.id,
        },
      }
    ),
  destroy: (id) =>
    model.category.destroy({
      where: {
        id: req.body.id,
      },
    }),
};
