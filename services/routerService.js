const repository = require('../repositories/routerRepository');

module.exports = {
  getAll: () => repository.getAll(),
  findById: (id) => repository.findById(id),
};
